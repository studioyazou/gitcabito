import {
  AsyncStorage,
} from 'react-native';

const keyDefault = '@cabito:default';

export default class StorageUtils {

  //================================//
  //=========== Divers =============//
  //================================//

  static async getDefault() {
    try {
      var favs = await AsyncStorage.getItem(keyDefault);

      if (favs === null)
        favs = [];
      else
        favs = JSON.parse(favs);

      return favs;
    } catch (error) {
      alert(error);
    }
    return null;
  }

  static async resetDefault() {
    try {
      await AsyncStorage.setItem(keyDefault, JSON.stringify([]));
      return [];
    } catch (error) {
      alert(error);
    }
    return null;
  }

  static async saveDefault(key) {
    try {
      var favs = await AsyncStorage.getItem(keyDefault);

      if (favs === null)
        favs = [];
      else
        favs = JSON.parse(favs);

      var index = -1;
      if ((index = favs.indexOf(key)) === -1)
        favs.push(key);
      else
        favs.splice(index, 1);
      await AsyncStorage.setItem(keyDefault, JSON.stringify(favs));
      return favs;
    } catch (error) {
      alert(error);
    }
    return null;
  }
}