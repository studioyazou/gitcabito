function Manga(name, url, isFav) {
  this.name = name;
  this.url = url;
  this.isFav = isFav;
  return this;
}

module.exports = Manga;