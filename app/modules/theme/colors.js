export const primary = '#2c59f5';
export const secondary = '#FFFFFF';
export const readable = "#FFFFFF"
export const text = '#1D1D1D';
export const separator = '#DDDDDD';
